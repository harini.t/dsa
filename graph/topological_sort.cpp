#include <iostream>
#include <vector>
#include <stack>
#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

void sort(matrix adj, int u, vector<bool> &visited, stack<int> &stk)
{

    visited[u] = true; // the node v is visited

    for (int v = 0; v < adj.size(); v++)
    {
        if (adj[u][v])
            // for all vertices v adjacent to u
            if (!visited[v])
                sort(adj, v, visited, stk);
    }
    stk.push(u); // push starting vertex into the stack
}

void TopologicalSort(matrix adj)
{
    int n = adj.size();
    stack<int> stk;
    vector<bool> visited(n, false);

    for (int i = 0; i < n; i++)
        visited[i] = false; // initially all nodes are unvisited

    for (int i = 0; i < n; i++)
        if (!visited[i]) // when node is not visited
            sort(adj, i, visited, stk);

    while (!stk.empty())
    {
        cout << stk.top() + 1 << " ";
        stk.pop();
    }
}

int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];
    cout << "Topological Sort of the given graph is: ";
    TopologicalSort(adj);
    return 0;
}