#include <iostream>
#include <vector>

#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;
matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
                adjlist[i].push_back(j);
    return adjlist;
}

vect degree(matrix adjList)
{
    int n = adjList.size();
    vect deg(n);
    for (int i = 0; i < n; i++)
        deg[i] = adjList[i].size();
    return deg;
}

int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix
    vector<bool> visited(n, false);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];
    matrix adjlst = adjacencyList(adj); // adjacency list
    vect degrees = degree(adjlst);
    for (int i = 0; i < degrees.size(); i++)
        cout << degrees[i] << " ";
    return 0;
}