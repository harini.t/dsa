#include <iostream>
#include <vector>
#include <stack>

#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

stack<int> s; // global stack

matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
                adjlist[i].push_back(j);
    return adjlist;
}

/*
---Steps---
DFS(u):
* Push u into stack, color[u] = 1, pre[u], low[u] = ++time
* try all neighbor v of u
* if color[v] = 0, DFS(v), low[u] = min{low[u],low[v]}
* else if color[v] = 1, low[u] = min{low[u],pre[v]}
* if low[u]==pre[u]
* Pop v from stack, color[v] = 2, until v=u
*/

void tarjan(int u, matrix g, vect &pre, vect &low, vector<bool> &visited, vect &scc)
{
    static int ticks = 0, current_scc = 0;
    pre[u] = low[u] = ticks++;
    s.push(u);
    visited[u] = true;
    const vector<int> &neighbors = g[u];
    for (int k = 0, m = neighbors.size(); k < m; ++k)
    {
        const int &v = neighbors[k];
        if (pre[v] == -1)
        {
            tarjan(v, g, pre, low, visited, scc);
            low[u] = min(low[u], low[v]);
        }
        else if (visited[v])
        {
            low[u] = min(low[u], low[v]);
        }
    }
    if (pre[u] == low[u])
    {
        int v;
        do
        {
            v = s.top();
            s.pop();
            visited[v] = false;
            scc[v] = current_scc;
        } while (u != v);
        current_scc++;
    }
}
int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix
    vect low(n, -1), pre(n, -1), scc(n, -1);
    vector<bool> visited(n);
    // get input
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];
    // adjacency list from matrix
    matrix adjlst = adjacencyList(adj);

    tarjan(0, adjlst, pre, low, visited, scc);

    cout << "Strongly connected components in the graph is:" << endl;

    for (int i = 0; i < n; i++)
    {
        if (scc[i] == scc[i + 1])
            cout << i + 1 << " ";
        else
            cout << i + 1 << endl;
    }
}