
#include <iostream>
#include <vector>
#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
                adjlist[i].push_back(j);
    return adjlist;
}

void DFS(matrix adjList, int v, vector<bool> &visited, vect &discover, vect &finish, int &time)
{
    visited[v] = true; // current node - visited
    discover[v] = ++time;
    for (int u : adjList[v]) // for all edges
        if (!visited[u])     // u not visited -> dfs
            DFS(adjList, u, visited, finish, discover, time);

    finish[v] = ++time;
}

int main()
{
    int n;
    cin >> n;               // number of nodes
    matrix adj(n, vect(n)); // n x n adjancency matrix
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    matrix adjList = adjacencyList(adj); // from adjancency matrix to adjacency list

    vector<bool> visited(n, false); // boolean array for visited nodes
    vect finish(n);                 // to track finish times
    vect discover(n);               // to track discovery times
    int time = 0;
    for (int i = 0; i < n; i++)
    {
        if (!visited[i])
        {
            DFS(adjList, i, visited, discover, finish, time);
        }
    }
    cout<<"The Discovery time is : ";
    for (int i = 0; i < n; i++)
        cout << discover[i] << " ";
    cout << "and The Finish time is : ";
    for (int i = 0; i < n; i++)
        cout<< finish[i] << " ";
    return 0;
}
