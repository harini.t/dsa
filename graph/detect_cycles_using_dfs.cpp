include<iostream>
#include <vector>
#define matrix vector<vector<int>>
#define vect vector<int>
    using namespace std;

matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
                adjlist[i].push_back(j);
    return adjlist;
}

void DFS(matrix adjList, int v, vector<bool> &visited, vect &finish, int &t)
{
    visited[v] = true; // current node - visited

    for (int u : adjList[v]) // for all edges
        if (!visited[u])     // u not visited -> dfs
            DFS(adjList, u, visited, finish, t);

    // backtrack & set finish t of v
    finish[v] = ++t;
}

bool hasCycle(matrix adjList, int n)
{
    vector<bool> visited(n, false); // boolean array for visited nodes

    vect finish(n); // to note finish ts

    int t = 0;

    // DFS to visit all connected components
    for (int i = 0; i < n; i++)
        if (!visited[i])
            DFS(adjList, i, visited, finish, t);

    for (int u = 0; u < n; u++)
        // check if (u, v) forms a back-edge.
        for (int v : adjList[u])
            if (finish[u] <= finish[v])
                return false;

    return true; // no back edges
}

int main()
{
    int n;
    cin >> n;               // number of nodes
    matrix adj(n, vect(n)); // n x n adjancency matrix
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    matrix adjList = adjacencyList(adj); // from adjancency matrix to adjacency list
    if (hasCycle(adjList, adjList.size()))
        cout << "Graph doesn't contain cycle";
    else
        cout << "Graph contains cycle";
    return 0;
}