#include <bits/stdc++.h>
#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

void bfs_traversal(matrix adj)
{
    int n = adj[0].size();
    queue<int> q;
    vector<bool> visited(n); // boolean array for visited nodes
    q.push(0); // starting node
    visited[0] = true; // starting from node 0 -> visited

    while (!q.empty())
    {
        int seen = q.front();
        cout << seen + 1 << " "; // print the currently seen node
        q.pop(); // remove the node from queue
        for (int i = 0; i < n; i++)
            if (adj[seen][i] == 1 && (!visited[i]))
            {
                q.push(i);
                visited[i] = true; // node is visited - True
            }
    }
}

int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    cout<< "BFS using adjacency matrix is : ";
    bfs_traversal(adj);
    return 0;
}
