#include <iostream>
#include <vector>

#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

void dfs_traversal(matrix adj, int start, vector<bool> &visited)
{
    int n = adj.size();
    cout << start + 1 << " "; // print current node
    visited[start] = true;    // starting node is visited

    for (int i = 0; i < adj[start].size(); i++)
    {
        if (adj[start][i] == 1 && (!visited[i])) // if not visited and adjacent node -> dfs
        {
            dfs_traversal(adj, i, visited); // recurrsion
        }
    }
}

int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix
    vector<bool> visited(n, false);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    cout << "BFS using adjacency matrix is : ";
    dfs_traversal(adj, 0, visited);
    return 0;
}
