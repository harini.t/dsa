#include <iostream>
#include <vector>
using namespace std;

void DFS(vector<int> adj[], int u, bool visited[], int disc[], int low[], int &time, int parent, bool isAP[])
{
    // Count of children in DFS Tree
    int children = 0;

    visited[u] = true; //  current node as visited

    disc[u] = low[u] = ++time;

    for (auto v : adj[u])
    {

        if (!visited[v])
        {
            children++;
            DFS(adj, v, visited, disc, low, time, u, isAP);

            low[u] = min(low[u], low[v]);

            if (parent != -1 && low[v] >= disc[u])
                isAP[u] = true;
        }

        // Update low value of u for parent node.
        else if (v != parent)
            low[u] = min(low[u], disc[v]);
    }

    // If u has two or more children.
    if (parent == -1 && children > 1)
        isAP[u] = true;
}

void articulation_point(vector<int> adj[], int V)
{
    int disc[V] = {0};
    int low[V];
    bool visited[V] = {false};
    bool isAP[V] = {false};
    int time = 0, par = -1;

    for (int u = 0; u < V; u++)
        if (!visited[u])
            DFS(adj, u, visited, disc, low, time, par, isAP);

    // Printing the APs
    for (int u = 0; u < V; u++)
        if (isAP[u] == true)
            cout << u + 1 << " ";
}

void addEdge(vector<int> adj[], int u, int v)
{
    adj[u].push_back(v);
    adj[v].push_back(u);
}

int main()
{
    // Create graphs given in above diagrams
    cout << "Articulation points in the graph are: ";
    int V;
    cin >> V;
    vector<int> adj[V];
    int x;
    for (int i = 0; i < V; i++)
        for (int j = 0; j < V; j++)
        {
            cin >> x;
            if (x == 1)
                addEdge(adj, i, j);
        }
    articulation_point(adj, V);
    return 0;
}

/*
5
0 1 1 1 0
0 0 1 0 0
0 0 0 0 0
0 0 0 0 1
0 0 0 0 0

Articulation points in the graph are: 1 4

*/