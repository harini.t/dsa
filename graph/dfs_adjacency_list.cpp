#include <iostream>
#include <vector>
#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
            {
                adjlist[i].push_back(j);
            }
    return adjlist;
}

void dfs_traversal(matrix adjlist, int node, vector<bool> &visited)
{
    vect adjnodes = adjlist[node];
    visited[node] = true;    // node -> visited
    cout << node + 1 << " "; // print currently seen node
    for (vect::iterator i = adjnodes.begin(); i != adjnodes.end(); ++i)
        if (!visited[*i])
            dfs_traversal(adjlist, *i, visited); // recursion - to reach the depth(last node)
}

int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n));         // n x n adjancency matrix
    vector<bool> visited(n, false); // boolean array for visited nodes

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    matrix adjlist = adjacencyList(adj); // get adjacency list from matrix
    cout << "DFS using adjacency list is : ";
    dfs_traversal(adjlist, 0, visited);
    return 0;
}
