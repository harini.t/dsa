#include <iostream>
#include <vector>

#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
                adjlist[i].push_back(j);
    return adjlist;
}

void inoutdegree(matrix adjlist)
{
    int n = adjlist.size();
    vect indeg(n), outdeg(n);
    for (int i = 0; i < n; i++)
    {
        // Out degree for Vi =  count of direct paths from i to other vertices
        outdeg[i] = adjlist[i].size();
        // Every vertex that has an incoming  edge from i
        for (int j = 0; j < adjlist[i].size(); j++)
            indeg[adjlist[i][j]]++;
    }

    for (int i = 0; i < n; i++)
    {
        cout << "In-degree of vertex " << i << " = " << indeg[i] << endl;
    }
    for (int i = 0; i < n; i++)
    {
        cout << "Out-degree of vertex " << i << " = " << outdeg[i] << endl;
    }
}

int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix
    vector<bool> visited(n, false);
    vect stk;
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];
    matrix adjlst = adjacencyList(adj);
    inoutdegree(adjlst);
    return 0;
}