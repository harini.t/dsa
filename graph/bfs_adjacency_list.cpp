#include <iostream>
#include <queue>
#include <vector>
#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
            {
                adjlist[i].push_back(j);
            }
    return adjlist;
}

void bfs_traversal(matrix adjlist)
{
    vector<bool> visited(adjlist.size(), false); // boolean array for visited nodes
    queue<int> q;
    visited[0] = true; // starting from node 0 -> visited
    q.push(0);         // starting node
    while (!q.empty())
    {
        int node = q.front();    // current node
        q.pop();                 // remove the node from queue
        cout << node + 1 << " "; // print the currently seen node
        for (int i = 0; i < adjlist[node].size(); i++)
        {
            int n = adjlist[node][i];
            if (!visited[n])
            {
                visited[n] = true; // node is visited - True
                q.push(n);         // enqueue the node
            }
        }
    }
}
int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    matrix adjlist = adjacencyList(adj); // get adjacency list from matrix
    cout << "BFS using adjacency list is : ";
    bfs_traversal(adjlist);
    return 0;
}
