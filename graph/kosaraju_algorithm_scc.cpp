#include <bits/stdc++.h>
#define matrix vector<vector<int>>
#define MAX_N 10
#define ll long long int
using namespace std;
int n, m;

struct Node
{
    vector<int> adj;
    vector<int> rev_adj;
};

Node g[MAX_N];

stack<int> S;
bool visited[MAX_N];

int component[MAX_N];
vector<int> components[MAX_N];
int numComponents;

void DFS(int x)
{
    visited[x] = true;
    for (int i = 0; i < g[x].adj.size(); i++)
    {
        if (!visited[g[x].adj[i]])
            DFS(g[x].adj[i]);
    }
    S.push(x);
}

void revDFS(int x)
{
    // printf("%d ", x);
    component[x] = numComponents;
    components[numComponents].push_back(x);
    visited[x] = true;
    for (int i = 0; i < g[x].rev_adj.size(); i++)
    {
        if (!visited[g[x].rev_adj[i]])
            revDFS(g[x].rev_adj[i]);
    }
}

void KosarajuAlgorithm()
{
    for (int i = 0; i < n; i++)
        if (!visited[i])
            DFS(i);

    for (int i = 0; i < n; i++)
        visited[i] = false;

    while (!S.empty())
    {
        int v = S.top();
        S.pop();
        if (!visited[v])
        {
            // printf("Component %d: ", numComponents);
            revDFS(v);
            numComponents++;
            cout << "\n";
        }
    }
}

void print(matrix A)
{
    for (int i = 0; i < A.size(); i++)
    {
        for (int j = 0; j < A.size(); j++)
            cout << A[i][j] << " ";
        cout << endl;
    }
}

int main()
{

    cin >> n;
    int a, b;
  
    matrix adj(n, vector<int>(n));
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
            {
                g[i].adj.push_back(j);
                g[j].rev_adj.push_back(i);
            }

    KosarajuAlgorithm();
    for (int i = 0; i < n; i++)
    {
        if (component[i] == component[i + 1])
            cout << i + 1 << " ";
        else
            cout << i + 1 << endl;
    }

    // for (int i = 0; i < n; i++)
    //     cout << component[i];

    return 0;
}