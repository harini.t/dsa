#include <iostream>
#include <list>
#include <stack>
using namespace std;
int count = 0;
class Edge
{
public:
    int u;
    int v;
    Edge(int u, int v);
};
Edge::Edge(int u, int v)
{
    this->u = u;
    this->v = v;
}

class Graph
{
    int V;
    int E;
    list<int> *adj;

    void BCCUtil(int u, int disc[], int low[],
                 list<Edge> *st, int parent[]);
    bool isBCUtil(int u, bool visited[], int disc[], int low[], int parent[]);

public:
    Graph(int V);               // Constructor
    void addEdge(int v, int w); // function to add an edge to graph
    void BCC();                 // prints strongly connected components
    bool isBC();                // to check if Biconnected graph
};

Graph::Graph(int V)
{
    this->V = V;
    this->E = 0;
    adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w);
    E++;
}

void Graph::BCCUtil(int u, int disc[], int low[], list<Edge> *st,
                    int parent[])
{

    static int time = 0;
    disc[u] = low[u] = ++time;
    int children = 0;
    list<int>::iterator i;
    for (i = adj[u].begin(); i != adj[u].end(); ++i)
    {
        int v = *i;
        // not visited yet
        if (disc[v] == -1)
        {
            children++;
            parent[v] = u;
            // store the edge in stack
            st->push_back(Edge(u, v));
            BCCUtil(v, disc, low, st, parent);

            low[u] = min(low[u], low[v]);

            // If u is an articulation point,
            // pop all edges from stack till u -- v
            if ((disc[u] == 1 && children > 1) || (disc[u] > 1 && low[v] >= disc[u]))
            {
                while (st->back().u != u || st->back().v != v)
                {
                    cout << st->back().u + 1 << "-" << st->back().v + 1 << " ";
                    st->pop_back();
                }
                cout << st->back().u + 1 << "-" << st->back().v + 1;
                st->pop_back();
                cout << endl;
                count++;
            }
        }

        else if (v != parent[u])
        {
            low[u] = min(low[u], disc[v]);
            if (disc[v] < disc[u])
            {
                st->push_back(Edge(u, v));
            }
        }
    }
}
bool Graph::isBCUtil(int u, bool visited[], int disc[], int low[], int parent[])
{
    // A static variable is used for simplicity, we can avoid use of static
    // variable by passing a pointer.
    static int time = 0;

    // Count of children in DFS Tree
    int children = 0;

    // Mark the current node as visited
    visited[u] = true;

    // Initialize discovery time and low value
    disc[u] = low[u] = ++time;

    // Go through all vertices adjacent to this
    list<int>::iterator i;
    for (i = adj[u].begin(); i != adj[u].end(); ++i)
    {
        int v = *i; // v is current adjacent of u

        // If v is not visited yet, then make it a child of u
        // in DFS tree and recur for it
        if (!visited[v])
        {
            children++;
            parent[v] = u;

            // check if subgraph rooted with v has an articulation point
            if (isBCUtil(v, visited, disc, low, parent))
                return true;

            // Check if the subtree rooted with v has a connection to
            // one of the ancestors of u
            low[u] = min(low[u], low[v]);

            // u is an articulation point in following cases

            // (1) u is root of DFS tree and has two or more children.
            if (parent[u] == -1 && children > 1)
                return true;

            // (2) If u is not root and low value of one of its child is
            // more than discovery value of u.
            if (parent[u] != -1 && low[v] >= disc[u])
                return true;
        }

        // Update low value of u for parent function calls.
        else if (v != parent[u])
            low[u] = min(low[u], disc[v]);
    }
    return false;
}

bool Graph::isBC()
{
    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    int *disc = new int[V];
    int *low = new int[V];
    int *parent = new int[V];

    // Initialize parent and visited, and ap(articulation point)
    //  arrays
    for (int i = 0; i < V; i++)
    {
        parent[i] = -1;
        visited[i] = false;
    }

    // Call the recursive helper function to find if there is an articulation
    // point in given graph. We do DFS traversal starting from vertex 0
    if (isBCUtil(0, visited, disc, low, parent) == true)
        return false;

    // Now check whether the given graph is connected or not. An undirected
    // graph is connected if all vertices are reachable from any starting
    // point (we have taken 0 as starting point)
    for (int i = 0; i < V; i++)
        if (visited[i] == false)
            return false;

    return true;
}

// The function to do DFS traversal. It uses BCCUtil()
void Graph::BCC()
{
    int *disc = new int[V];
    int *low = new int[V];
    int *parent = new int[V];
    list<Edge> *st = new list<Edge>[E];

    // Initialize disc and low, and parent arrays
    for (int i = 0; i < V; i++)
    {
        disc[i] = -1;
        low[i] = -1;
        parent[i] = -1;
    }

    for (int i = 0; i < V; i++)
    {
        if (disc[i] == -1)
            BCCUtil(i, disc, low, st, parent);

        int j = 0;
        // If stack is not empty, pop all edges from stack
        while (st->size() > 0)
        {
            j = 1;
            cout << st->back().u + 1 << "-" << st->back().v + 1 << " ";
            st->pop_back();
        }
        if (j == 1)
        {
            cout << endl;
            count++;
        }
    }
}

// Driver program to test above function
int main()
{
    int n, x;
    cin >> n;
    Graph g(n);

    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
        {
            cin >> x;
            if (x == 1)
                g.addEdge(i, j);
        }

    if (g.isBC())
    {
        cout << "Graph is Biconnected and Biconnected components are:";
        g.BCC();
    }
    else
        cout << "Graph is not Biconnected";
    return 0;
}

/*
2
0 1
0 0

Graph is Biconnected and Biconnected components are:1-2

3
0 1
1 2

Graph is not Biconnected

*/