#include <iostream>
#include <vector>
#define matrix vector<vector<int>>
#define vect vector<int>
using namespace std;

matrix adjacencyList(matrix adj)
{
    int n = adj.size();
    matrix adjlist(n);
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            if (adj[i][j] == 1)
                adjlist[i].push_back(j);
    return adjlist;
}
// Perform DFS on the graph and set the departure time of all vertices of the graph
void DFS(matrix adjList, int v, vector<bool> &visited, vect &finish, int &time)
{
    // mark the current node as visited
    visited[v] = true;

    // do for every edge (v, u)
    for (int u : adjList[v])
        // if `u` is not yet visited
        if (!visited[u])
            DFS(adjList, u, visited, finish, time);

    // ready to backtrack
    // set departure time of vertex `v`
    finish[v] = time++;
}

// Returns true if given directed graph is DAG
bool isDAG(matrix adjList, int n)
{
    vector<bool> visited(n, false); // boolean array for visited nodes

    vect finish(n); // keep track of finish time of a vertex in DFS

    int time = 0;

    // Perform DFS from all undiscovered vertices to visit all connected components of a graph
    for (int i = 0; i < n; i++)
        if (!visited[i])
            DFS(adjList, i, visited, finish, time);

    // check if the given directed graph is DAG or not
    for (int u = 0; u < n; u++)
        for (int v : adjList[u]) // check if (u, v) forms a back-edge.

            /* If the finish time of vertex v >= finish time of u, they form a back edge.
               Note that finish[u] will be equal to finish[v] only if `u = v`, i.e., vertex contain an edge to itself */
            if (finish[u] <= finish[v])

                return false;

    // no back edges
    return true;
}

int main()
{
    int n;
    cin >> n;
    matrix adj(n, vect(n)); // n x n adjancency matrix
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            cin >> adj[i][j];

    matrix adjList = adjacencyList(adj);
    if (isDAG(adjList, adjList.size()))
        cout << "Graph doesn't contain cycle";
    else
        cout << "Graph contains cycle";
    return 0;
}

/*

Test 1: 1
--- Input ---
4
0 1 1 0
0 0 0 0
0 0 0 1
0 1 0 0

--- Program output ---
Graph doesn't contain cycle

--- Expected output (text)---
BFS using adjacency list is : 1 2 3 4

Test 5: 5
--- Input ---
3
0 1 1
1 0 0
0 0 1

--- Program output ---
Graph contains cycle

Summary of tests
+------------------------------+
| 2 tests run/ 2 tests passed |
+------------------------------+
*/