## DSA
Data structures and algorithms programs

## Graph Algorithms Implemented
 - [BFS Traversal with adjacency matrix](https://gitlab.com/harini.t/dsa/-/blob/main/graph/bfs_adjacency_list.cpp)
 - [BFS Traversal with adjacency list](https://gitlab.com/harini.t/dsa/-/blob/main/graph/bfs_adjacency_matrix.cpp)
 - [DFS Traversal with adjacency matrix](https://gitlab.com/harini.t/dsa/-/blob/main/graph/dfs_adjacency_list.cpp)
 - [DFS Traversal with adjacency list](https://gitlab.com/harini.t/dsa/-/blob/main/graph/bfs_adjacency_matrix.cpp)
 - [Discovery and Finish Times of Vertices](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/dicovery_finish_times.cpp)
 - [Verify a DAG](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/dicovery_finish_times.cpp)
 - [Topological Sort](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/dicovery_finish_times.cpp)
 - [Kosaraju's algorithm for SCC](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/dicovery_finish_times.cpp)
 - [Detect cycles using DFS](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/detect_cycles_using_dfs.cpp)
 - [Degree of vertices in Undirect graph](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/degree_undirected.cpp)
 - [In and Out Degree of vertices in Direct graph](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/degree_directed.cpp)
 - [Tarjan's algorithm for SCC](https://gitlab.com/harini.t/dsa/-/blob/68d28956afdc878285372aac4a2deb51037aaaea/graph/tarjan_algorithm_scc.cpp)



